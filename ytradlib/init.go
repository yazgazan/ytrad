
package ytradlib

import (
  "os"
  "strings"
)

// Struct Used to define the targeted languages.
// The lang should follow the Google translate format
// (en for english, fr for french ...).
type Langs struct{
  In  string
  Out string
}

// CheckArgs verifies the command line arguments,
// it basically just check the number of arguments.
func CheckArgs() error {
  if len(os.Args) < 4 {
    return Usage()
  }
  return nil
}

// GetLangs create and fill the Langs struct with command line args.
func GetLangs() Langs {
  return Langs{
    os.Args[1],
    os.Args[2],
  }
}

// GetInText reconstruct the input text by joining the args with '%20' as glue.
func GetInText() string {
  return strings.Join(os.Args[3:], "%20")
}

