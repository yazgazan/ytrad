
package ytradlib

import (
  "net/http"
  "io/ioutil"
  "encoding/json"

  "fmt"
)

// Contain the results from google. It must match their api's format since we just unmarshal their response.
type GoogleResult struct{
  Data struct{
    Translations []struct{
      TranslatedText string
    }
  }
}

// Generate the api call, basically just a sprintf
func GenerateGoogleUrl(langs Langs, text string, key string) string {
  return fmt.Sprintf("https://www.googleapis.com/language/translate/v2?key=%s&q=%s&source=%s&target=%s", key, text, langs.In, langs.Out)
}

// Get and unmarshal the google results.
func GetGoogleResults(langs Langs, text string, key string) (GoogleResult, error) {
  var ret GoogleResult

  url := GenerateGoogleUrl(langs, text, key)
  resp, err := http.Get(url)

  if err != nil {
    return GoogleResult{}, &ErrorString{
      "Couldn't get Google's results.",
    }
  }
  defer resp.Body.Close()
  content, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    return GoogleResult{}, &ErrorString{
      "Couldn't get Google's results, error reading the response.",
    }
  }
  json.Unmarshal(content, &ret)
  return ret, nil
}

