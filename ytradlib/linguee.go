
package ytradlib

import (
  "fmt"

  "github.com/PuerkitoBio/goquery"
)

// Single entry, the entry's value is stored in the Entries map in LingueeSection.
type LingueeEntry struct{
  // May be 'v', 'adj', etc.
  Type string
  // stored as percentage
  Frequence int
}

type LingueeSection struct{
  Name string
  // May be 'verb', 'adj', etc.
  Type string
  Entries map[string]LingueeEntry
}

// Linguee Results, Filled by the GetLingueeResults function.
type LingueeResults struct{
  // There are two types of results provided by linguee, either exact or inexact.
  // Though the html formating is the same for both.
  Inexact   bool
  Sections  []LingueeSection
}

// Maps the google lang format to the linguee's one.
// Not all the googles lang are supported by linguee.
func GetLingueeLang(lang string) (string, error) {
  langs := map[string]string{
    "en": "english",
    "es": "spanish",
    "de": "german",
    "pt": "portuguese",
    "it": "italian",
    "ru": "russian",
    "jp": "japanese",
    "ch": "chinese",
    "pl": "polish",
    "nl": "dutch",
    "fr": "french",
  }
  ret, ok := langs[lang]

  if !ok {
    return "", &ErrorString{
      fmt.Sprintf("Can't match '%s' to a Linguee suported language.", lang),
    }
  }
  return ret, nil
}

// Generate the url call to get Linguee's results.
func GenerateLingueeUrl(langs Langs, text string) (string, error) {
  inLang, err := GetLingueeLang(langs.In)
  if err != nil {
    return "", err
  }

  outLang, err := GetLingueeLang(langs.Out)
  if err != nil {
    return "", err
  }

  return fmt.Sprintf("http://www.linguee.com/%s-%s/search?source=auto&query=%s", inLang, outLang, text), nil
}

// Process the linguee's pie class name and convert it to percentages.
func LingueeProcessPieResults(pie *goquery.Selection) int {
  var freq int = -1

  if pie.Size() != 0 {
    if pie.HasClass("pie_0of8") {
      freq = 0 * 100 / 8
    } else if pie.HasClass("pie_1of8") {
      freq = 1 * 100 / 8
    } else if pie.HasClass("pie_2of8") {
      freq = 2 * 100 / 8
    } else if pie.HasClass("pie_3of8") {
      freq = 3 * 100 / 8
    } else if pie.HasClass("pie_4of8") {
      freq = 4 * 100 / 8
    } else if pie.HasClass("pie_5of8") {
      freq = 5 * 100 / 8
    } else if pie.HasClass("pie_6of8") {
      freq = 6 * 100 / 8
    } else if pie.HasClass("pie_7of8") {
      freq = 7 * 100 / 8
    } else if pie.HasClass("pie_8of8") {
      freq = 8 * 100 / 8
    }
  }
  return freq
}

// Uses goquery to get and parse the linguees results.
// You shouldn't touch this code without a developper console
// opened on linguee's website.
func GetLingueeResults(langs Langs, text string) (LingueeResults, error) {
  var currentSection int = 0
  var results LingueeResults
  var numSections int

  url, err := GenerateLingueeUrl(langs, text)
  if err != nil {
    return results, err
  }

  doc, err := goquery.NewDocument(url)

  if err != nil {
    return results, &ErrorString{
      "Couldn't get Linguee's results.",
    }
  }
  // counting sections
  numSections = doc.Find("#dictionaryPane div.dictentry.left").Size()
  results.Sections = make([]LingueeSection, numSections)
  if doc.Find("div.inexact").Size() == 0 {
    results.Inexact = false
  } else {
    results.Inexact = true
  }
  for index := range(results.Sections) {
    results.Sections[index].Entries = make(map[string]LingueeEntry)
  }
  // filling sections
  doc.Find("#dictionaryPane div.dictentry").Each(func (i int, s *goquery.Selection) {
    if s.HasClass("left") == true { // new Section
      if results.Sections[currentSection].Name != "" {
        currentSection++
      }
      results.Sections[currentSection].Name = s.Find("span.dictentry").Text()
      results.Sections[currentSection].Type = s.Find("span.semantic_field").Text()
      return
    } else if s.HasClass("right") == true { // new Entries
      s.Find("span.smalldictentry").Each(func (j int, se *goquery.Selection) {
        pie := se.Find("span.pie")
        value := se.Find("a.smalldictentry").Text()
        _type := se.Find("span.semantic_field").Text()
        freq := LingueeProcessPieResults(pie)
        results.Sections[currentSection].Entries[value] = LingueeEntry{
          _type,
          freq,
        }
      })
    }
  })
  return results, nil
}

