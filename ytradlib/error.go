
package ytradlib

import (
  "fmt"
  "os"
)

// Very basic error handling, may be improved in the future.
type ErrorString struct{
  Msg string
}

// Just Returns the error message.
func (s *ErrorString) Error() string {
  return s.Msg
}

// Generate the usage message.
func Usage() *ErrorString {
  return &ErrorString{
    fmt.Sprintf("Usage : %s <in lang> <out lang> text...", os.Args[0]),
  }
}

