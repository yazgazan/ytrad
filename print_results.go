
package main

import (
  "fmt"
  /* "strings" */
  /* "io/ioutil" */

  "github.com/yazgazan/ytrad/ytradlib"
  /* "code.google.com/p/go.text/encoding" */
  /* "code.google.com/p/go.text/encoding/charmap" */
  /* "code.google.com/p/go.text/transform" */
)

/* func LingueeConvertCharset(str string) string { */
/*   // charmap.ISO8859_15 : encoding from */
/*   reader := strings.NewReader(str) */
/*   converter := transform.NewReader(reader, charmap.ISO8859_15.NewEncoder()) */
/*   ret, err := ioutil.ReadAll(converter) */

/*   if err != nil { // encoding error -> forget about encoding */
/*     return str */
/*   } */
/*   return string(ret) */
/* } */

// Format the linguee's results for printing.
// Still working on encoding since linguee's encoding is iso-8859-15 ...
func PrintLingueeResults(results ytradlib.LingueeResults, err error) {
  if err != nil {
    fmt.Println(err.Error())
    return
  }

  fmt.Print("www.linguee.com")
  if results.Inexact == true {
    fmt.Print(" (Inexact)")
  }
  fmt.Println(" :")
  for _, section := range(results.Sections) {
    fmt.Print("\t" + section.Name)
    if len(section.Type) != 0 {
      fmt.Print(" [", section.Type, "]")
    }
    fmt.Println("")
    for name, entry := range(section.Entries) {
      /* utf8_name := LingueeConvertCharset(name) */
      fmt.Print("\t\t" + name)
      if len(entry.Type) != 0 {
        fmt.Print(" [", entry.Type, "]")
      }
      if entry.Frequence != -1 {
        fmt.Print(" (", entry.Frequence, "%)")
      }
      fmt.Println("")
    }
  }
}

// Format the google's result for printing.
func PrintGoogleResults(results ytradlib.GoogleResult, err error) {
  if err != nil {
    fmt.Println(err.Error())
    return
  }

  fmt.Println("Google :")
  for _, translations := range results.Data.Translations {
    fmt.Println("\t" + translations.TranslatedText)
  }
}

