
YTrad
=====

# About

YTrad is a go wrapper around translation tools. It currently supports Google Translate and Linguee.

Check [the documentation](http://godoc.org/github.com/yazgazan/ytrad/ytradlib) to use the lib.

# Install

To install it as a command, execute `go get -u github.com/yazgazan/ytrad`

To enable Google Translate, you need to generate a developper Key in the `auth > Credentials > Public Api Access` from the Google developers console.
Once you have your key, past it in the `github.com/yazgazan/ytrad/config.go` file and run `go install`

# TODO

- Get the utf-8 conversion to work for linguee
- Create an aggregator
- Support more translation tools

