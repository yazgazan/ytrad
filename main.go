
// bin package for ytradlib.
// Google developer key must be put im config.go
package main

import (
  "fmt"

  "github.com/yazgazan/ytrad/ytradlib"
)

func main() {
  var err error
  var langs ytradlib.Langs
  var inText string

  if err = ytradlib.CheckArgs(); err != nil {
    fmt.Println(err.Error())
    return
  }

  langs = ytradlib.GetLangs()
  inText = ytradlib.GetInText()
  if len(C_GoogleKey) != 0 {
    PrintGoogleResults(ytradlib.GetGoogleResults(langs, inText, C_GoogleKey))
  }
  PrintLingueeResults(ytradlib.GetLingueeResults(langs, inText))
}

